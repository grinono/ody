import redirect from "./redirect";
import ext from "../utils/ext";
import storage from "../utils/storage";

export function checkIfPageNeedReloadIfReload(tabId, host, redirectPage) {
  storage.get("visited", function(result) {
    if (result.visited) {
      // When the host already has redirected, then how long ago this was
      if (result.visited === host) {
        // redirect once to the other page.
      } else {
        redirect(tabId, redirectPage, host);
      }
    } else {
      // Redirect
      redirect(tabId, redirectPage, host);
    }
  });
}
