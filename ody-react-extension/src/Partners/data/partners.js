// extension on install partners

export let partners = [
  {
    label: "KiKa",
    value: "www.kika.nl",
    host: "www.kika.nl",
    folder: "kika"
  },
  {
    label: "Knalunteren",
    host: "www.knalunteren.nl",
    value: "www.knalunteren.nl",
    folder: "knalunteren"
  },
  {
    label: "Reuma Nederland",
    host: "reumanederland.nl",
    value: "reumanederland.nl",
    folder: "reumaNederland"
  },
  {
    label: "Care Nederland",
    host: "www.carenederland.org",
    value: "www.carenederland.org",
    folder: "care"
  },
  {
    label: "Clini Clowns",
    host: "www.cliniclowns.nl",
    value: "www.cliniclowns.nl",
    folder: "cliniClowns"
  },
  {
    label: "Rode Kruis",
    host: "www.rodekruis.nl",
    value: "www.rodekruis.nl",
    folder: "rodeKruis"
  },
  {
    label: "Unicef",
    host: "www.unicef.nl",
    value: "www.unicef.nl",
    folder: "unicef"
  },
  {
    label: "Compassion",
    host: "www.compassion.nl",
    value: "www.compassion.nl",
    folder: "compassion"
  },
  {
    label: "Amnesty",
    host: "www.amnesty.nl",
    value: "www.amnesty.nl",
    folder: "amnesty"
  },
  {
    label: "Greenpeace",
    host: "www.greenpeace.org",
    value: "www.greenpeace.org",
    folder: "greenpeace"
  },
  {
    label: "Wereld Natuur Fonds",
    host: "www.wwf.nl",
    value: "www.wwf.nl",
    folder: "wwf"
  },
  {
    label: "Hartstichting",
    value: "www.hartstichting.nl",
    host: "www.hartstichting.nl",
    folder: "hartStichting"
  },
  {
    label: "Amref",
    value: "www.amref.nl",
    host: "www.amref.nl",
    folder: "amref"
  },
  {
    label: "Red een kind",
    value: "www.redeenkind.nl",
    host: "www.redeenkind.nl",
    folder: "redEenKind"
  },
  {
    label: "TreesPlease",
    value: "www.treesplease.online",
    host: "www.treesplease.online",
    folder: "treesplease"
  }
];
