// import { daisycon } from './redirectStragegies/daisycon'
import ext from "../utils/ext";
import storage from "../utils/storage";
import { NotificationPartnerAwareness } from "../Notifications/notifyOfPartner";

// the redirect setup is just a single redirect at first page visit. the storage is removed at every browser startup.
export function redirect(tabId, redirectPage, currentHost, link) {
  storage.get("visited", function(result) {
    let visitedAffiliates = new Set(result.visited);

    if (!visitedAffiliates.has(currentHost)) {
      // "page will initate a notification and a redirect now "
      NotificationPartnerAwareness(currentHost);
      ext.tabs.update(tabId, {
        url: redirectPage.deepLink
      });
      setTimeout(function() {
        ext.tabs.update(tabId, {
          url: link
        });
      }, 2000);
    }
    // add the host to the already refreshed domains
    visitedAffiliates.add(currentHost);
    storage.set({ visited: [...visitedAffiliates] });
  });
}
