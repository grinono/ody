import React, { useState } from "react";
import storage from "../../utils/storage";
import Select from "react-select";
import "./card.css";
import {
  Card,
  Button,
  CardHeader,
  CardFooter,
  CardBody,
  CardTitle,
  CardText
} from "reactstrap";

import { partners } from "../../Partners/data/partners";

export default () => {
  // const [partner, setPartner] = useState(null);
  const [partnerList, setPartnerList] = useState(null);
  const [partnerState, setPartnerState] = useState(false);

  function handleChange(selectedOption) {
    setPartnerList(selectedOption);
    storage.set({
      installPartner: selectedOption.value,
      installDetails: selectedOption
    });
  }

  function getPartner() {
    storage.get("installPartner", result => {
      partners.forEach(partnerDetails => {
        if (partnerDetails.value === result.installPartner) {
          setPartnerList(partnerDetails);
        }
      });
    });
  }
  getPartner();
  storage.get("viaPartner", result => {
    if (result.viaPartner) {
      setPartnerState(true);
    }
  });
  return (
    <div style={{ width: 300, height: 300 }}>
      <Card>
        <CardHeader tag="h3">Shop & Share</CardHeader>
        <CardBody>
          <CardTitle> Ody, Je helpt nu jouw goede doel</CardTitle>
          <CardText>
            Wanneer jij online aankopen doet bij een aangesloten webshop,
            betaalt de webshop een commissie. Door de sponsorknop gaat dat geld
            naar je onderstaande doel.
          </CardText>
          <Button
            color="info"
            onClick={() => window.open("https://www.ody.social/")}
          >
            Meer informatie
          </Button>
        </CardBody>
        <CardFooter className="text-muted">
          <Select
            value={partnerList}
            onChange={handleChange}
            options={partners}
            isDisabled={partnerState}
          />
        </CardFooter>
      </Card>
    </div>
  );
};
