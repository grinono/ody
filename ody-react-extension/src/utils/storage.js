import ext from "./ext";

var value = ext.storage.sync ? ext.storage.sync : ext.storage.local;
export default value;
