import ReactGA from "react-ga";

const trackingId = "UA-103854620-4"; // Replace with your Google Analytics tracking ID

ReactGA.initialize(trackingId, {
  debug: true,
  titleCase: false,
  cookieDomain: null,
  appName: "Sevi extension"
});

ReactGA.ga("set", "checkProtocolTask", null);
ReactGA.ga("set", "checkStorageTask", null);
// ReactGA.ga("set", "appId", "app.id");
// ReactGA.ga("set", "appName", "the name");
// ReactGA.ga("set", "appVersion", 2321);
// ReactGA.ga("set", "dimension1", 1.44);
ReactGA.pageview("/install");
// ReactGA.initialize(trackingId);

// console.log("ReactGA:", ReactGA);
// ReactGA.set({
//   userId: "this user"
//   // any data that is relevant to the user session
//   // that you would like to track with google analytics
// });
// ReactGA.pageview("/initialized");
// console.log("ReactGA:", ReactGA);

export default ReactGA;
