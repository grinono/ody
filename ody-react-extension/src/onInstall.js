// import { detectPartner } from './Partners/partnerDetection'
import { createBookmark } from "./bookmarks/create";
import ext from "./utils/ext";
import ReactGA from "./analytics";
import URL from "url-parse";
import { partners } from "./Partners/data/partners";
import storage from "./utils/storage";
// import ga from "./analytics";
import { SetNewIcons } from "./installPartners/setIcons";

export function onInstall() {
  detectPartner();
  // createBookmark()
}

export function detectPartner() {
  ext.tabs.query({}, function(tabs) {
    for (let tab of tabs) {
      let parseURL = new URL(tab.url);
      for (let partner of partners) {
        if (parseURL.host === partner.host) {
          //
          // Send install partner to analytics.
          //

          ReactGA.event({
            category: "Install",
            action: partner.label,
            label: "first install"
          });

          //
          // save install partner site to the localhost installPartner
          //

          SetNewIcons(partner);
          storage.set({
            installPartner: parseURL.host,
            viaPartner: true,
            installDetails: partner
          });
        }
      }
    }
  });
}
