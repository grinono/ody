import ext from "../utils/ext";
import storage from "../utils/storage";

export function NotificationPartnerAwareness(currentHost) {
  storage.get("installDetails", function(installPartner) {
    ext.notifications.clear("34kjlkjklas", clear => {});
    const folder =
      installPartner &&
      installPartner.installDetails &&
      installPartner.installDetails.folder
        ? `${installPartner.installDetails.folder}/`
        : null;
    const partner =
      installPartner &&
      installPartner.installDetails &&
      installPartner.installDetails.label
        ? installPartner.installDetails.label
        : "je doel";
    const opt = {
      type: "basic",
      title: "Ody partner",
      iconUrl: `./assets/icons/${folder}icon-128.png`,
      message: `${currentHost} sponsor ${partner} wanneer je hier iets koopt!`
    };
    // launche notification
    ext.notifications.create("34kjlkjklas", opt, err => {
      console.log(err);
    });
  });
}
