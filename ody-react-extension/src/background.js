import ext from "./utils/ext";
import storage from "./utils/storage";
import "./analytics";
// 'use strict'
// import { thankYouPage } from "./Notifications/thanks.js";
import { partnerDetection } from "./Partners/partnerDetection";

import { onInstall } from "./onInstall";

// Remove all saved redirects
storage.set({ visited: [""] });

setInterval(function() {
  storage.set({ visited: [""] });
}, 2 * 60 * 60 * 100);

//
// Check URL changes
//

ext.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  //   thankYouPage(tab.url);
  partnerDetection(tabId, tab.url);
});

ext.tabs.onCreated.addListener(function(tab) {
  // thankYouPage(tab.url)
  partnerDetection(tab.url);
});

//
// Brower extension is installed from what page?
//

// onInstall event handler
ext.runtime.onInstalled.addListener(function(object) {
  if (object.reason === "install") {
    onInstall();
  }
});
