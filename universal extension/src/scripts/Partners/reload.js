import redirect from './redirect'
import ext from "../utils/ext";
import storage from "../utils/storage";

export function checkIfPageNeedReloadIfReload (tabId, host, redirectPage) {
    storage.get('visited', function (result) {
      // console.log(result)
      if (result.visited) {
        // When the host already has redirected, then how long ago this was
        if (result.visited === host) {
          // console.log(host)
          console.log('we should not redirect, we just did one')
          // redirect once to the other page.
          // console.log(tabId + redirectPage);
        } else {
          redirect(tabId, redirectPage, host)
        }
      } else {
        // Redirect
        console.log('we should redirect, as their is nothing in storage')
        redirect(tabId, redirectPage, host)
      }
    })
  }