// extension on install partners

export let partners = [
  {
    siteName: "KiKa",
    host: "www.kika.nl"
  },
  {
    siteName: "Knalunteren",
    host: "www.knalunteren.nl"
  },
  {
    siteName: "Hartstichting",
    host: "www.hartstichting.nl"
  },
  {
    siteName: "Reuma Nederland",
    host: "reumanederland.nl"
  },
  {
    siteName: "Care Nederland",
    host: "www.carenederland.org"
  },
  {
    siteName: "Clini Clowns",
    host: "www.cliniclowns.nl"
  },
  {
    siteName: "Rode Kruis",
    host: "www.rodekruis.nl"
  },
  {
    siteName: "Unicef",
    host: "www.unicef.nl"
  },
  {
    siteName: "Compassion",
    host: "www.compassion.nl"
  },
  {
    siteName: "Amnesty",
    host: "www.amnesty.nl"
  },
  {
    siteName: "Greenpeace",
    host: "www.greenpeace.org"
  },
  {
    siteName: "Wereld Natuur Fonds",
    host: "www.wwf.nl"
  },
  {
    siteName: "Hart Stichting",
    host: "www.hartstichting.nl",
    imageLink: "./images/shared/icons/hartStichting"
  }
];
