import URL from 'url-parse'
import { AllLinks } from './data/main.js'
import { redirect } from './redirect'

//
// Check if the URL visited is know as a affiliate
//

export function partnerDetection (tabId, link) {
  const currentVisitingURL = new URL(link)
  // console.log('hallo')
  
  const currentHost = currentVisitingURL.host.replace(/^www\./,'')
  const allAffiliateLinks = AllLinks()
  for (const affiliate of allAffiliateLinks) {
    // console.log(affiliate)
    // Check if host of currentPage is known in DB records
    if (affiliate.host === currentHost) {
      console.log('affiliate detected')
      // select redirect strategy
      redirect(tabId, affiliate, currentHost, link)
    }
  }
}