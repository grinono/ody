import {
  data as thankyouURL
} from '../Partners/data/partners'
import ext from "./utils/ext";
import storage from "./utils/storage";
import URL from 'url-parse'

// let parsedURL = new Set()

// thankyouURL.forEach((site) => {
//   let url = new URL(site.thankYouURL)
//   // console.log(site)
//   parsedURL.add({'thankYouURL': url, 'notificationImageLocation': site.notificationImageLocation})
// })

export function thankYouPage (link) {
  let visitedURL = new URL(link)
  for (let dataURL of parsedURL) {
    // console.log(dataURL.thankYouURL.pathname)
    // console.log(visitedURL.pathname)
    if (dataURL.thankYouURL.hostname === visitedURL.hostname && dataURL.thankYouURL.pathname === visitedURL.pathname) {
      // console.log('current page is same as notification path')
      // console.log(dataURL.notificationImageLocation)
      // window.browser.storage.local.get('installPartner', function (result) {
        ext.notifications.create('12', getNotificationTemplate(dataURL.notificationImageLocation, result), () => {
          setTimeout(function () {
            ext.notifications.clear('12')
          // }, 10000)
        })
      })
    }
  }
}

function getNotificationTemplate (imageURL, partner) {
  // console.log(partner.installPartner)
  if (partner.installPartner === 'www.knalunteren.nl') {
    return {
      type: 'image',
      title: 'Bedankt voor je gift',
      imageUrl: imageURL,
      message: 'Je hebt zojuist tot 8% van je aankoop aan KNA Lunteren bijgedragen. Top!',
      iconUrl: '/images/notifications/kna.jpg',
      requireInteraction: true
    }
  }
  if (partner.installPartner === 'www.kika.nl') {
    return {
      type: 'image',
      title: 'Bedankt voor je gift',
      imageUrl: imageURL,
      message: 'Je hebt zojuist tot 8% van je aankoop aan KiKa bijgedragen. Top!',
      iconUrl: '/images/notifications/yfc.png',
      requireInteraction: true
    }
  }
}
