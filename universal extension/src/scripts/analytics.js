import GoogleAnalytics from 'ga'

const ua = 'UA-142627399-1'
const host = 'www.ody.social'
const ga = new GoogleAnalytics(ua, host)

export default ga