// import { detectPartner } from './Partners/partnerDetection'
import { createBookmark } from "./bookmarks/create";
import ext from "./utils/ext";
import URL from "url-parse";
import { partners } from "./Partners/data/partners";
import storage from "./utils/storage";
import ga from "./analytics";
import { SetNewIcons } from "./installPartners/setIcons";

export function onInstall() {
  detectPartner();
  // createBookmark()
}

export function detectPartner() {
  ext.tabs.query({}, function(tabs) {
    for (let tab of tabs) {
      let parseURL = new URL(tab.url);
      console.log("parseURL:", parseURL);
      for (let partner of partners) {
        // console.log(partner.host);
        if (parseURL.host === partner.host) {
          console.log(` ${partner.host} it is the install partner`);
          // save install partner site to the localhost installPartner
          SetNewIcons(partner);
          storage.set({ installPartner: parseURL.host });
          // track installs in Google analytics
          console.log("send to google");
          ga.trackEvent({
            category: "Install",
            action: parseURL.host,
            label: "one more install"
          });
        }
      }
    }
  });
}
